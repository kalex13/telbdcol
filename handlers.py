import aiohttp
import asyncio
import json
from aiohttp import web

from utils import get_config


config = get_config()

class Api(object):
    URL = 'https://api.telegram.org/bot%s/%s'

    def __init__(self, token, loop):
        self._token = token
        self._loop = loop

    async def _request(self, method, message):
        headers = {
            'Content-Type': 'application/json'
        }
        async with aiohttp.ClientSession(loop=self._loop) as session:
            async with session.post(self.URL % (self._token, method),
                                    data=json.dumps(message),
                                    headers=headers) as resp:
                try:
                    assert resp.status == 200
                except:
                    print(f'Error response status: {resp.status}')

    async def sendMessage(self, chatId, text):
        message = {
            'chat_id': chatId,
            'text': text,
            'entities': [
                {
                    "type": "italic",
                    "offset": 0,
                    "length": 6
                }
            ]
        }
        print('=='*10)
        print(message)
        print('=='*10)
        await self._request('sendMessage', message)


class Conversation(Api):
    def __init__(self, token, loop):
        super().__init__(token, loop)

    async def _handler(self, message):
        pass

    async def handler(self, request):
        message = await request.json()
        print('***' * 20)
        for i in message['message']:
            print(f"{i}: {message['message'][i]}")
        print('***' * 20)
        if message['message'].get('text') == '/start':
            asyncio.ensure_future(self._start(message['message']))
        elif message['message'].get('text') == '/help':
            asyncio.ensure_future(self._start(message['message']))
        else:
            asyncio.ensure_future(self._handler(message['message']))
        return aiohttp.web.Response(status=200)


class EchoConversation(Conversation):

    def __init__(self, token, loop):
        super().__init__(token, loop)
        self.description = "/help - Show help\n/start - Start\n/register - Register"

    async def _handler(self, message):
        await self.sendMessage(message['chat']['id'],
                               message['text'],)

    async def _start(self, message):
        await self.sendMessage(message['chat']['id'],
                               self.description)


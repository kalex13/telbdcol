from handlers import EchoConversation


def setup_routes(app, loop):
    echobot = EchoConversation(app['config']['telegram']['token'], loop)
    app.router.add_post('/api/v1/coll', echobot.handler)
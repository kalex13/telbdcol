#!/usr/bin/env python3

import asyncio
from aiohttp import web
import ssl

from routes import setup_routes
from utils import get_config


def get_ssl_context(config):
    ssl_context = ssl.create_default_context(purpose=ssl.Purpose.CLIENT_AUTH)
    ca_file = config['ca_file']
    key_file = config['key_file']
    ssl_context.load_cert_chain(certfile=ca_file, keyfile=key_file)
    return ssl_context


async def init_app(loop):
    app = web.Application(loop=loop, middlewares=[])
    app['config'] = get_config()
    setup_routes(app, loop)
    return app

if __name__ == '__main__':
    config = get_config()
    loop = asyncio.get_event_loop()
    try:
        app = loop.run_until_complete(init_app(loop))
        web.run_app(app,
                    host=app['config']['net']['host'],
                    port=int(app['config']['net']['port']),
                    ssl_context=get_ssl_context((app['config']['ssl'])))
    except Exception as e:
        print('Error create server: %r' % e)
    finally:
        pass
    loop.close()
